package ru.sre.autoscheduler.scheduler

import java.io.FileReader
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import io.circe.yaml.parser
import ru.sre.autoscheduler.httpClient.OnCallClientService
import ru.sre.autoscheduler.httpClient.model.{Credentials, UserExistError}
import ru.sre.autoscheduler.model
import zio._

trait Scheduler {
  def scheduleScript(yamlFile: String): Task[Unit]
}

final case class SchedulerImpl(client: OnCallClientService) extends Scheduler {
  private def createEvents(creds: Credentials, user: model.User, teamName: String, timezone: String): Task[Unit] =
    for {
      _ <- ZIO.foreachDiscard(user.duty) { duty =>
        val date   = LocalDate.parse(duty.date, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        val zoneId = ZoneId.of(timezone)
        val offset = zoneId.getRules().getOffset(LocalDateTime.now());
        client.createEvent(
          creds,
          date.atStartOfDay().toEpochSecond(offset),
          date.plusDays(1).atStartOfDay().toEpochSecond(offset),
          user.name,
          teamName,
          duty.role
        )
      }
    } yield ()

  private def createUser(creds: Credentials, user: model.User): Task[Unit] =
    (for {
      _ <- client.createUser(creds, user.name)
      _ <- client.updateUser(creds, user.name, user.fullName, user.email, user.phoneNumber)
    } yield ()).catchSome{
      case _: UserExistError =>  ZIO.logInfo(s"User already exist. Ignore creating user")
    }

  private def createTeamSchedule(creds: Credentials, team: model.Team): Task[Unit] =
    for {
      _ <- client.createTeam(creds, team.name, team.schedulingTimezone, team.email, team.slackChannel)
      _ <- ZIO.foreachDiscard(team.users)(u => createUser(creds, u))
      _ <- ZIO.foreachDiscard(team.users.map(_.name))(userName => client.addUserInTeam(creds, userName, team.name))
      _ <- ZIO.foreachDiscard(team.users)(user => createEvents(creds, user, team.name, team.schedulingTimezone))
    } yield ()

  override def scheduleScript(yamlFile: String): Task[Unit] = ZIO.scoped {
    (for {
      _ <- ZIO.logInfo("Script started")
      reader <- ZIO.acquireRelease(
        ZIO.attempt(new FileReader(yamlFile))
      )(r => ZIO.attempt(r.close()).ignore)
      json     <- ZIO.attempt(parser.parse(reader)).absolve
      schedule <- ZIO.attempt(json.as[model.Schedule]).absolve
      _        <- ZIO.logInfo("Getting credentials")
      creds    <- client.login()
      _        <- ZIO.foreachDiscard(schedule.teams)(team => createTeamSchedule(creds, team))
      _ <- ZIO.logInfo("Script finished")
    } yield ()).catchAll(e => client.logout() *> ZIO.fail(e))
  }
}

object Scheduler {
  val layer: URLayer[OnCallClientService, Scheduler] = ZLayer.fromFunction(SchedulerImpl.apply _)
}
