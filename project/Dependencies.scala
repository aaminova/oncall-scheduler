import sbt._
import sbt.librarymanagement.ModuleID

object Dependencies {
  object Version {
    val zio       = "2.0.17"
    val zioConfig = "4.0.0-RC16"
    val client    = "3.8.15"
    val circe     = "0.14.1"
    val derevo    = "0.13.0"
  }

  val zio: Seq[ModuleID] = Seq(
    "dev.zio" %% "zio"         % Version.zio,
    "dev.zio" %% "zio-streams" % Version.zio,
  )

  val circe: Seq[ModuleID] = Seq(
    "io.circe" %% "circe-core" % Version.circe,
    "io.circe" %% "circe-yaml" % Version.circe
  )

  val derevo: Seq[ModuleID] = Seq(
    "tf.tofu" %% "derevo-circe" % Version.derevo,
  )

  val zioConfig: Seq[ModuleID] = Seq(
    "dev.zio" %% "zio-config",
    "dev.zio" %% "zio-config-typesafe",
    "dev.zio" %% "zio-config-magnolia",
  ).map(_ % Version.zioConfig)

  val client: Seq[ModuleID] = Seq(
    "com.softwaremill.sttp.client3" %% "zio"          % Version.client,
    "com.softwaremill.sttp.client3" %% "slf4j-backend" % Version.client,
    "com.softwaremill.sttp.client3" %% "circe"         % Version.client,
  )

}
