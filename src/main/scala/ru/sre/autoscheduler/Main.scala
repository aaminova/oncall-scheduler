package ru.sre.autoscheduler

import ru.sre.autoscheduler.httpClient.OnCallClientService
import ru.sre.autoscheduler.scheduler
import zio._

object Main extends ZIOAppDefault {

  def run = (for {
    scheduler <- ZIO.service[scheduler.Scheduler]
    r <- getArgs
      .flatMap {
        case Chunk(yamlFile) => scheduler.scheduleScript(yamlFile)
        case _               => Console.printLine("Usage: <program> <yamlFile>") *> exit(ExitCode.failure)
      }
  } yield r)
    .provideSome(AppConfigProvider.setup, OnCallClientService.layer, scheduler.Scheduler.layer)

}
