package ru.sre.autoscheduler.httpClient.model

import derevo.circe.decoder
import derevo.derive

@derive(decoder)
final case class Credentials(
    token: String,
    cookie: String,
)
