package ru.sre.autoscheduler.httpClient.model

sealed trait OnCallError extends Exception

final case class UserExistError(userName: String) extends OnCallError
