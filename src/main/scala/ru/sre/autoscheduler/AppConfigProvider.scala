package ru.sre.autoscheduler

import com.typesafe.config.ConfigFactory
import zio._
import zio.config.typesafe.TypesafeConfigProvider

object AppConfigProvider {
  val setup: TaskLayer[Unit] = ZLayer(
    ZIO.attempt(TypesafeConfigProvider.fromTypesafeConfig(ConfigFactory.load().getConfig("ru.sre.autoscheduler")))
  ).flatMap(env => Runtime.setConfigProvider(env.get))
}
