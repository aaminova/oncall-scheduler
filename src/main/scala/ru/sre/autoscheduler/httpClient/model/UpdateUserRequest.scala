package ru.sre.autoscheduler.httpClient.model

import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder)
final case class UpdateUserRequest(
    name: String,
    fullName: String,
    contacts: Contacts
)

@derive(snakeEncoder)
final case class Contacts(call: String, email: String)
