package ru.sre.autoscheduler.model

import derevo.circe.snakeDecoder
import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder, snakeDecoder)
final case class User(
    name: String,
    fullName: String,
    phoneNumber: String,
    email: String,
    duty: List[Duty]
)
