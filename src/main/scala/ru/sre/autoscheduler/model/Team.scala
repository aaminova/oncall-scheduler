package ru.sre.autoscheduler.model

import derevo.circe.snakeDecoder
import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder, snakeDecoder)
final case class Team(
    name: String,
    schedulingTimezone: String,
    email: String,
    slackChannel: String,
    users: List[User]
)
