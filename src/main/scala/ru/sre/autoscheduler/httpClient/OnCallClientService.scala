package ru.sre.autoscheduler.httpClient

import io.circe.Json
import io.circe.syntax._
import ru.sre.autoscheduler.configuration.OnCallConfig
import ru.sre.autoscheduler.httpClient.model._
import sttp.client3._
import sttp.client3.circe._
import sttp.client3.httpclient.zio.HttpClientZioBackend
import sttp.model.StatusCode
import zio._

trait OnCallClientService {
  def login(): Task[Credentials]
  def logout(): Task[Unit]
  def createTeam(creds: Credentials, name: String, timezone: String, email: String, channel: String): Task[Unit]
  def createUser(creds: Credentials, name: String): Task[Unit]
  def updateUser(creds: Credentials, name: String, fullName: String, email: String, phone: String): Task[Unit]
  def addUserInTeam(creds: Credentials, name: String, team: String): Task[Unit]
  def createEvent(creds: Credentials, start: Long, end: Long, user: String, team: String, role: String): Task[Unit]
}

final case class OnCallClientServiceImpl(config: OnCallConfig, backend: SttpBackend[Task, Any])
    extends OnCallClientService {

  override def login(): Task[Credentials] = for {
    response <- backend
      .send(
        basicRequest
          .post(uri"${config.uri}/login")
          .body(("username", config.username), ("password", config.password))
          .response(asJson[Json])
      )
    body = response.body
    token <- ZIO
      .succeed(body.flatMap(b => b.hcursor.downField("csrf_token").as[String]).toOption)
      .someOrFail(new NoSuchElementException("No csrf-token"))
    cookie <- ZIO
      .succeed(response.unsafeCookies.find(_.name == "oncall-auth"))
      .someOrFail(new NoSuchElementException("No cookie"))
    _ <- ZIO.logInfo(s"Authorized")
  } yield Credentials(token = token, cookie = s"${cookie.name}=${cookie.value}")

  override def logout(): Task[Unit] = for {
    response <- backend
      .send(
        basicRequest
          .post(uri"${config.uri}/logout")
          .response(asJson[Json])
      )
    _ <- ZIO.when(!response.code.isSuccess) {
      ZIO.logError("Unable to logout") *> ZIO.fail(new NoSuchElementException("No csrf-token"))
    }
    _ <- ZIO.logInfo(s"Logout")
  } yield ()

  override def createTeam(creds: Credentials, name: String, timezone: String, email: String, channel: String): Task[Unit] =
    for {
      body <- ZIO.succeed(
        CreateTeamRequest(name = name, schedulingTimezone = timezone, email = email, slackChannel = channel)
      )
      response <- backend
        .send(
          basicRequest
            .post(uri"${config.uri}/api/v0/teams")
            .header("x-csrf-token", creds.token)
            .header("Cookie", creds.cookie)
            .body(body.asJson)
            .response(asJson[Json])
        )
      _ <- response.code match {
        case code if code.isSuccess => ZIO.logInfo(s"Team $name created")
        case code if code == StatusCode.UnprocessableEntity =>
          ZIO.logInfo(s"Team $name already created") *> ZIO.fail(
            new RuntimeException(s"Unable to create team: $name, already created")
          )
        case _ =>
          ZIO.logError(s"Unable to create team $name") *>
            ZIO.fail(
              new RuntimeException(s"Unable to create team: $name, body: ${response.body}")
            )
      }
    } yield ()

  override def createUser(creds: Credentials, name: String): Task[Unit] =
    for {
      body <- ZIO.succeed(CreateUserRequest(name = name))
      response <- backend
        .send(
          basicRequest
            .post(uri"${config.uri}/api/v0/users")
            .header("x-csrf-token", creds.token)
            .header("Cookie", creds.cookie)
            .body(body.asJson)
            .response(asJson[Json])
        )
      _ <- response.code match {
        case code if code.isSuccess => ZIO.logInfo(s"User $name created")
        case code if code == StatusCode.UnprocessableEntity =>
          ZIO.logInfo(s"User $name already created") *> ZIO.fail(
            UserExistError(name)
          )
        case _ =>
          ZIO.logError(s"Unable to create user $name") *>
            ZIO.fail(
              new RuntimeException(s"Unable to create user: $name, body: ${response.body}")
            )
      }
    } yield ()

  override def updateUser(
      creds: Credentials,
      name: String,
      fullName: String,
      email: String,
      phone: String
  ): Task[Unit] =
    for {
      body <- ZIO.succeed(
        UpdateUserRequest(name = name, fullName = fullName, contacts = Contacts(call = phone, email = email))
      )
      response <- backend
        .send(
          basicRequest
            .put(uri"${config.uri}/api/v0/users/$name")
            .header("x-csrf-token", creds.token)
            .header("Cookie", creds.cookie)
            .body(body.asJson)
            .response(asJson[Json])
        )
      _ <- response.code match {
        case code if code.isSuccess => ZIO.logInfo(s"User $name updated")
        case code if code == StatusCode.NotFound =>
          ZIO.logInfo(s"User $name not found") *> ZIO.fail(
            new RuntimeException(s"Unable to update user: $name, not found")
          )
        case _ =>
          ZIO.logError(s"Unable to update user $name") *>
            ZIO.fail(
              new RuntimeException(s"Unable to update user: $name, body: ${response.body}")
            )
      }
    } yield ()

  override def addUserInTeam(creds: Credentials, name: String, team: String): Task[Unit] =
    for {
      body <- ZIO.succeed(AddTeamUserRequest(name = name))
      response <- backend
        .send(
          basicRequest
            .post(uri"${config.uri}/api/v0/teams/$team/users")
            .header("x-csrf-token", creds.token)
            .header("Cookie", creds.cookie)
            .body(body.asJson)
            .response(asJson[Json])
        )
      _ <- response.code match {
        case code if code.isSuccess => ZIO.logInfo(s"User $name added to a team $team")
        case _ =>
          ZIO.logError(s"Unable to add user $name to the team $team") *>
            ZIO.fail(
              new RuntimeException(s"Unable to add user $name to the team $team, body: ${response.body}")
            )
      }
    } yield ()

  override def createEvent(
      creds: Credentials,
      start: Long,
      end: Long,
      user: String,
      team: String,
      role: String
  ): Task[Unit] =
    for {
      body <- ZIO.succeed(CreateEventRequest(start = start, end = end, user = user, team = team, role = role))
      response <- backend
        .send(
          basicRequest
            .post(uri"${config.uri}/api/v0/events")
            .header("x-csrf-token", creds.token)
            .header("Cookie", creds.cookie)
            .body(body.asJson)
            .response(asJson[Json])
        )
      _ <- response.code match {
        case code if code.isSuccess =>
          ZIO.logInfo(s"Event created: user=$user; team=$team, role=$role, body=${response.body}")
        case _ =>
          ZIO.logError(s"Unable to create event for user=$user; team=$team, role=$role") *>
            ZIO.fail(
              new RuntimeException(
                s"Unable to create event for user=$user; team=$team, role=$role, body: ${response.body}"
              )
            )
      }
    } yield ()
}

object OnCallClientService {
  val layer: TaskLayer[OnCallClientService] = ZLayer(
    for {
      backend <- HttpClientZioBackend()
      config  <- ZIO.config(OnCallConfig.config)
    } yield OnCallClientServiceImpl(config, backend)
  )
}
