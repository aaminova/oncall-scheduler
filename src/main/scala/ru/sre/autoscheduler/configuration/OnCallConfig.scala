package ru.sre.autoscheduler.configuration

import zio.Config
import zio.config.magnolia._

final case class OnCallConfig(uri: String, username: String, password: String)

object OnCallConfig {
  val config: Config[OnCallConfig] = deriveConfig[OnCallConfig].nested("oncall")
}
