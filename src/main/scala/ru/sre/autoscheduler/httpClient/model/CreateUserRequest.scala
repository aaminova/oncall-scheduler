package ru.sre.autoscheduler.httpClient.model

import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder)
final case class CreateUserRequest(
    name: String,
    active: Int = 1
)
