package ru.sre.autoscheduler.httpClient.model

import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder)
final case class CreateEventRequest(
    start: Long,
    end: Long,
    user: String,
    team: String,
    role: String
)
