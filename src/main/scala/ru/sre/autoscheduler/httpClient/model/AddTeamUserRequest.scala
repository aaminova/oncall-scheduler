package ru.sre.autoscheduler.httpClient.model

import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder)
final case class AddTeamUserRequest(name: String)
