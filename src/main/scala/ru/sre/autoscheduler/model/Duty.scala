package ru.sre.autoscheduler.model

import derevo.circe.snakeDecoder
import derevo.circe.snakeEncoder
import derevo.derive
import java.time.LocalDate

@derive(snakeEncoder, snakeDecoder)
final case class Duty(
    date: String,
    role: String
)
