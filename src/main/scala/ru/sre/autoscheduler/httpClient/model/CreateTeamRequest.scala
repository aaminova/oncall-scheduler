package ru.sre.autoscheduler.httpClient.model

import derevo.circe.snakeEncoder
import derevo.derive

@derive(snakeEncoder)
final case class CreateTeamRequest(
    name: String,
    schedulingTimezone: String,
    email: String,
    slackChannel: String
)
